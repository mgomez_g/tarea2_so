#include<stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include<string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include<stdbool.h>
#include "listas.h" 
int nVenta = 0; // variable para calcular el numero de impresiones 
int counter = 0;
int *limite; // nos indicara hasta donde debemos imprimir 
bool condition; // bool de condicion para el ciclo 
int userStatus; 
int a=0,b=0,c=0; // estos contadores son para trackear el estado de los semaforos 

void *lector();
void *funcionImpresora(); // funciones de los hilos con nombres representativos 
sem_t Impresora, servidor,user; // semaforos
pthread_t usuario, impresora; // hilos 

int main(int argc, char *argv[]){
    limite =(int *)malloc(sizeof(int));
    sem_init(&user ,0 ,0);
    sem_init(&servidor ,0 ,1);//no habilitado
    sem_init(&Impresora ,0 ,0);//no habilitado
    b=1;
    condition=true;
    
    listaImpresiones= (struct Lista*)malloc(sizeof(lista));  // inicializo la lista 
    inicializacion(listaImpresiones); 
    userStatus = pthread_create(&usuario, NULL, (void *)lector, NULL);  // creo hilo 
    *limite = -1;
    usleep(200);
    pthread_create(&impresora, NULL, (void *) funcionImpresora, NULL); // creo segundo hilo de impresora 
    usleep(200);
    printf("[servidor]%d\n",listaImpresiones->size);
    while(condition){
        sem_wait(&servidor); //pausamoe el proceso servidor
        //printf("[servidor]\tEntro a servidor\n");
        //printf("[servidor] usuario = %d | servidor = %d | impresora = %d\n\n",a,b,c);
        //printf("[Servidor] EL NUMERO DE ELEMENTOS EN LA COLA ES DE %d\n", listaImpresiones->size);
        b-=1; 
        counter = listaImpresiones->size;
        *limite = posicionUrgencia();   // lo primero que hacemos es fijarnos en la urgencia del pedido 
            // en caso de ser urgente retornara el valor en la posicion en la que se encuentra la urgencia 
            // si se encuentra en un valor mayor a 3 la ignoraremos 
        if (*limite!=-1 && *limite<3){
            
            //printf("[Servidor] Se encontro urgencia en la posicion %d\n",*limite);
            usleep(100);                                     
            // -----hacemos pausas para aegurar el funcionamiento y mejor compilacion 
            //printf("[Servidor]limite = %d \n",*limite);
            c+=1;
            //printf("[servidor]Voy a impresora\n");

            sem_post(&Impresora); // vamos al proceso impresora
            
        }
        else if(counter>=3){

            *limite = 3; // el limite nos dira hasta que parte de la cola debemos imprimir , en este caso 3 elementos 
            //printf("[Servidor]limite = %d \n",*limite);
            
            usleep(100);
            c+=1;
            //printf("[servidor]Voy a impresora\n");
            sem_post(&Impresora); // vamos al proceso impresora
            
        } else if(a == -1){
            sem_post(&Impresora);
            condition = false;
            
        }else{
            *limite =-1; // cuando el limite es -1 no debemos imprimir nada 
            a+=1;
            //printf("[servidor]Voy a usuario\n");
            sem_post(&user); // vamos al proceso usuario
            
        }
    }
   
    pthread_join(usuario,NULL);  // esperamos a que terminen los otros procesos 
    pthread_join(impresora,NULL);
    sem_destroy(&servidor);
    sem_destroy(&Impresora);  // destruimos semaforos
    printf("------->No se pueden imprimir mas archivos \nSe termina el proceso principal\n");
      
    return 0;
}

void *lector(){
    FILE *fp;
    char texto[50];
    fp = fopen("cola.txt", "r");
    
    char *tipo = NULL,*urgencia = NULL;
    int hojas = 0;
    while(true){
        sem_wait(&user);
        if (fp==NULL){
            printf("El archivo de productos no se puede abrir\n");
        }else{
            //printf("[usuario] usuario = %d | servidor = %d | impresora = %d\n\n",a,b,c);
            //printf("[usuario]\tEntro a usuario\n");
            a-=1;
            if(!feof(fp)){  // lectura del archivo  y aignacion de valores para cada elemento de la lista 
                //printf("[usuario] usuario = %d | servidor = %d | impresora = %d\n\n",a,b,c);
                fgets(texto,50,fp); // obtengo linea a linea
                // asigno espacio de memoria para estructura
                tipo = strtok(texto," ,"); // obtengo la descripcion del producto
                tipo = strdup(tipo); // asigno memoria especifica ya que es de tipo puntero (char*)
                hojas = atoi(strtok(NULL, " ,"));  // obtengo el resto de los elementos y los paso a enteros (int)
                urgencia = strtok(NULL, " ,");
                urgencia = strdup(urgencia);            
                nuevaImpresion(tipo,urgencia,hojas); // inserto elemento en la lista
                //printf("[Usuario]-----------------Cola de impresion----------------- \n");
                //mostrarCola();
                if(*tipo=='B'){  // muestra de los elementos leidos del archivo de texto 
                    printf("\n\n[Usuario]------------------------------------------------------------------------------------------------------\n");
                    printf("[Usuario]tipo de impresion : Blanco y negro | Se esperan %d micro seg. | urgencia : %c | Cantidad de Paginas : %d \n",hojas*100,*urgencia,hojas);
                    printf("[Usuario]---------------------------------------------------------------------------------------------------------- \n");
                    usleep(hojas*100); 
                }else if(*tipo == 'C'){
                    printf("\n\n[Usuario]------------------------------------------------------------------------------------------------------\n");
                    printf("[Usuario]tipo de impresion : color | Se esperan %d micro seg. | urgencia : %c | Cantidad de Paginas : %d \n",hojas*200,*urgencia,hojas);
                    printf("[Usuario]---------------------------------------------------------------------------------------------------------- \n");
                    usleep(hojas*200);
                }
                b+=1;
                //printf("[usuario]\tvoy a servidor\n");
                sem_post(&servidor);   // una vez leido un elemento pasamos al servidor para evaluar

                
            }else if(feof(fp) && listaImpresiones->size!=0){  // si el archivo ha sido leido por completo pero aun o se imprimen todos los elementos
               // printf("[Estado de los hilos] usuario = %d | servidor = %d | impresora = %d\n\n",a,b,c);
                a -=1;
                printf("[usuario] No hay mas pedidos para agregar a la cola\n");
                printf("[usuario] Quedan %d impresiones pendientes\n", listaImpresiones->size);
                printf("[usuario] Termino de proceso\n");
                b+=1;
                sem_post(&servidor);
                fclose(fp);
                a= -1;
                pthread_exit(NULL);
            }else{ // si ya se leyeron e imprimieron todos los elementos 
                printf("[usuario] No hay mas pedidos para agregar a la cola y el tamaño de la lista es de 0\n");
                printf("[usuario] Termino de proceso\n");
                b+=1;
                sem_post(&servidor); // entramos al servidor para terminar el resto de procesos 
                a= -1;
                fclose(fp);
                pthread_exit(NULL); // salimos del hilo y cerramos 
            }

        }
        
    }
    a= -1;
    fclose(fp);
    pthread_exit(NULL);
    
} 

void *funcionImpresora(){
    imp *p = listaImpresiones->inicio, *aux =NULL;
    while(true){
        sem_wait(&Impresora);
        imp *p = listaImpresiones->inicio;
        //printf("[impresora]\tEntro a impresora\n");
        int i =0;
        if(a == -1){  // -1 en el semaforo de usuario sera nuestro codigo de salida de los hilos
            pthread_exit(NULL);
        }else if (p==NULL){
            printf("[impresora]La lista esta vacía\n");
           // printf("[impresora]\tvoy a usuario\n");
            sem_post(&user); // volvemos a leer el archivo 
        }else{
            //printf("El limite es %d\n",(*limite));
            if(*limite > -1){ // si tenemos ina indicacion de una cantidad de elementos a imprimir , imprimimos ...
                c-=1;
             //   printf("[Estado de los hilos] usuario = %d | servidor = %d | impresora = %d\n\n",a,b,c);
                printf("[impresora] inicio\n");
                printf("[impresora] imprimiendo %d elementos ...\n\n",*limite);
           //     printf("[impresora] Limite = %d\n", *limite);
                i= 0;
                for ( i; i < *limite; i++){
                    nVenta +=1;
                    usleep(150*p->hojas);
                    printf("----------------------------------------------------\n");
                    printf("[impresora]Impresion [%d] lista\n", nVenta);
                    printf("[impresora]El pedido tardo [%d] micro seg.\n", 150*p->hojas); // esperaremos segun cantidad de hojas 
                    if(*(p->tipo)=='B'){
                        printf("El total de la impresion es : $%d \n\n", (p->hojas)*10); // 10 para b y n 
                        
                    }else if(*(p->tipo) == 'C'){
                        printf("El total de la impresion es : $%d \n\n",  (p->hojas)*20);// 20 para color 
                    }
        
                    aux = p->next;  // cambiamos a siguiente elemento de la lista 
                    free(p); // borramos el elemento ya impreso 
                    p = aux;
                    listaImpresiones->inicio = aux;
                    listaImpresiones->size -=1;
                }
                b +=1;
                sem_post(&servidor);
            }else if(*limite == -1){
                printf("[impresora] Esperando mas pedidos \n");
                usleep(150);
                a +=1;
                sem_post(&servidor);  // pasamos al servidor 
            }
        }
        
    }
    pthread_exit(NULL);
}
