#ifndef LISTAS_H
#define LISTAS_H
typedef struct Impresion {
    char *tipo,*urgencia;
    int hojas;
    struct Impresion *next;
    }imp;

typedef struct Lista{
    imp *inicio;
    imp *final;
    int size;
    int total;
}lista;

lista *listaImpresiones;
imp *header;
void mostrarCola();
void nuevaImpresion(char *tipo, char *urgancia,int hojas);
void inicializacion (lista *header);

#endif