# Tarea2_SO



## Integrantes :
* *Nombre :* Mariapaz Gómez.
* *ROL:* 201930043-1.

## Instrucciones para ejecutar :
Correr los siguientes comandos 
```
gcc -pthread  main.c listas.c -o tarea2 -g
```
* El -g es para que se pueda hacer debbugg.
* debemos incluir -pthread para correr los programas con hilos.

## Supuestos :
Puesto que no se dice que metodología utilizar, suponemos que debemos utilizar semaforos para la sincronia de los hilos y el roceso principal .
