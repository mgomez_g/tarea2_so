#include "listas.h"

#include<stdio.h>
#include<string.h>
#include <sys/types.h>
#include <unistd.h>


void inicializacion(lista *listaProductos){
    listaImpresiones->inicio = NULL;
    listaImpresiones->final = NULL;
    listaImpresiones->size = 0;
    listaImpresiones->total = 0;
}

void mostrarCola(){
        imp *aux = NULL;
        aux = listaImpresiones->inicio;
        while (aux!=NULL){   
            printf("Tipo : %s\n", aux->tipo);
            printf("Cantidad de Hojas : %d\n", aux->hojas);
            printf("Urgencia : %s\n", aux->urgencia);
            
            aux= aux->next;
        }
        printf("[Usuario]El largo de la cola es de %d elementos\n", listaImpresiones->size);
}

void nuevaImpresion(char *tipo, char *urgencia,int hojas){
    imp *im = (imp *)malloc(sizeof(imp)); // asigno memoria para nuevo elemento de la lista 
    if(listaImpresiones->inicio == NULL){ // si la lista esta vacia
        im->tipo = tipo; // asigno valores a atributos de la estructura 
        im->urgencia = urgencia;
        im->hojas = hojas;
        im->next = NULL; // el siguiente aun no esta dado, por lo que inicializo en NULL
        listaImpresiones->inicio = im; // asigno el inicio de la lista como primer elemento 
        listaImpresiones->size ++; // aumento el tamaño en 1
        listaImpresiones->final = im; // apunto el final de la lista al primer elemento (podria dejarse en NULL en este caso no genera diferencias)
        header = im; //apunto el header a la cabeza de la lista
    } else if(header == NULL){  // si el header no esta asignado correctamente 
        printf("El puntero no contiene una direccion \n");
    } else{   // si ya hay al menos un elemento en la lista
        header->next = im; // apunto el puntero next a el nuevo elemento creado 
        im->tipo = tipo; // asigno valores a atributos de la estructura 
        im->urgencia = urgencia;
        im->hojas = hojas;
        im->next = NULL; // inicializo el puntero next en NULL
        listaImpresiones->final = im; // apunto al ultimo elemento creado como el ultimo de la lista
        listaImpresiones->size ++; // aumento el tamaño
        header = im; // asigno el header como el ultimo elemento creado
    }
}
int posicionUrgencia(){
    int posicion = 1;
    imp *p = listaImpresiones->inicio;
    
    if (p==NULL){
        printf("La lista de productos esta vacía");
    }
    else{
        while (p!=NULL){  
            if(*(p->urgencia) == 'S'){ // strcmp retorna 0 si los strings son iguales
                printf("[Servidor] Se encontro una urgencia en la posicion %d de la cola\n",posicion);
                return posicion;
            }else{
                posicion+=1;
                p = p->next;
            }
        }
    }
    //printf("[Servidor] No se encontraron urgencias\n");
    posicion=-1;
    return posicion;
}




